/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package genbankreader;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * The class that opens the files.
 * @author prfolkersma
 */
public class FileOpener {
    /**
     * opens the file.
     * @param file the path of the file
     * @return complete gbk file in string format
     */
        public final String openFile(final Path file) {
            String complete = "";
            Charset charset = Charset.forName("US-ASCII");
            try (BufferedReader reader = Files.newBufferedReader(file,
                    charset)) {
                String line;
                while ((line = reader.readLine()) != null) {
                    complete += line + "\r";
                }
            } catch (IOException x) {
                System.err.println("There is no file to open."
                        + " You may have typed it wrong");
                System.exit(0);
            }
            return complete;
        }
}
