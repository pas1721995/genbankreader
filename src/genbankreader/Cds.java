package genbankreader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * the Cds class that saves all the info about the found cds.
 */

public class Cds {
    /**
     * The product of the cds.
     */
    private String product;
    /**
     * the coordinate of the cds.
     */
    private String[] coordinate;
    /**
     * the db reference of the cds.
     */
    private ArrayList<String> dbxref;
    /**
     * the translation of the cds.
     */
    private String translation;
    /**
     * the constructor of the cds class.
     * @param cdsInfo the cdsinfo
     */
    public Cds(final String cdsInfo) {
        setProduct(cdsInfo);
        setCoordinate(cdsInfo);
        setDbXref(cdsInfo);
        setTranslation(cdsInfo);
    }
    /**
     * sets the product.
     * @param cdsInfo the cdsinfo
     */
    private void setProduct(final String cdsInfo) {
        Pattern productRegex = Pattern.compile("/product(.*?)/protein",
                Pattern.DOTALL);
        Matcher productMatcher = productRegex.matcher(cdsInfo);
        while (productMatcher.find()) {
            String prodInfo = (cdsInfo.substring(productMatcher.start(),
                    productMatcher.end()));
            prodInfo = prodInfo.split("/product=")[1].split("/protein")[0];
            product = prodInfo;
        }
        product = product.trim();
    }
    /**
     * sets thecoordinate.
     * @param cdsInfo the cdsinfo
     */
    private void setCoordinate(final String cdsInfo) {
        Pattern coordianteRegex = Pattern.compile("CDS  (.*?)/locus",
                Pattern.DOTALL);
        Matcher coordinateMatcher = coordianteRegex.matcher(cdsInfo);
        while (coordinateMatcher.find()) {
            String coordInfo = (cdsInfo.substring(coordinateMatcher.start(),
                    coordinateMatcher.end()));
            coordInfo = coordInfo.split("CDS")[1].split("/locus")[0].trim();
            if (coordInfo.contains("complement")) {
                coordInfo = coordInfo.split("complement\\(")[1];
            }
            if (coordInfo.contains("join")) {
                coordInfo = coordInfo.split("join\\(")[1];
            }
            if (coordInfo.contains(")")) {
                coordInfo = coordInfo.split("\\)")[0];
            }
            if (coordInfo.contains(",")) {
                String[] coordinate1 = coordInfo.split(",")[0].split("\\.\\.");
                String[] coordinate2 = coordInfo.split(",")[1].split("\\.\\.");
                String[] coords = {coordinate1[0], coordinate2[1]};
                coordinate = coords;
            } else {
                coordinate = coordInfo.split("\\.\\.");
            }

        }
    }
    /**
     * sets the dbxref.
     * @param cdsInfo the cdsinfo
     */
    private void setDbXref(final String cdsInfo) {
        ArrayList dbxrefs = new ArrayList<>();
        Pattern dbRegex = Pattern.compile("/db_xref(.*)");
        Matcher dbMatcher = dbRegex.matcher(cdsInfo);
        while (dbMatcher.find()) {
            String dbInfo = (cdsInfo.substring(dbMatcher.start(),
                    dbMatcher.end()));
            dbxrefs.add(dbInfo.split("/db_xref=")[1]);
        }
        dbxref = dbxrefs;
    }
    /**
     * sets the translation.
     * @param cdsInfo the cdsinfo
     */
    private void setTranslation(final String cdsInfo) {
        String translationInfo = cdsInfo.split("/translation=")[1];
        translationInfo = translationInfo.split("gene")[0];
        String[] listTranslation = translationInfo.split("\\r");
        for (String string : listTranslation) {
            translation += string.trim();
        }
        translation = translation.split("null")[1];
    }

    @Override
    public final String toString() {
        return "Cds{" + "product=" + product.trim() + ", coordinate="
                 + Arrays.toString(coordinate) + ", dbxref=" + dbxref
                + ", translation=" + translation + '}';
    }
    /**
     * gets the product of the cds.
     * @return a cds product
     */
    public final String getProduct() {
        return product;
    }
    /**
     * gets the coordinate of the cds.
     * @return a cds coordinate
     */
    public final String[] getCoordinate() {
        return coordinate;
    }
    /**
     * gets the translation of the cds.
     * @return a cds translation
     */
    public final String getTranslation() {
        return translation;
    }

}

