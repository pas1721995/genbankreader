package genbankreader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * the class that executes the options.
 */
public class Executer {
    /**
     * the end list for the program.
     */
    private ArrayList<Object> returnList;

    /**
     * the class that handels the options.
     *
     * @param option the option for the program
     * @param input the input given
     * @param genes all the genes in the gbk file
     * @param organism the organism the gbk file is about
     * @param cdses all the cdses in the gbk file
     */
    public Executer(final String option,
            final String input,
            final ArrayList<Gene> genes,
            final Organism organism,
            final ArrayList<Cds> cdses) {
        if ("g".equals(option)) {
            returnList = fetchGene(input, genes);
        }
        if ("c".equals(option)) {
            returnList = fetchCds(input, cdses);
        }
        if ("f".equals(option)) {
            returnList = fetchFeatures(input, cdses, genes);
        }
        if ("s".equals(option)) {
            returnList = findSites(input, organism, cdses, genes);
        }
        if ("u".equals(option)) {
            returnList = null;
        }

    }

    /**
     * fetches all the genes with a given gene name.
     * @param input a name of genes
     * @param genes a list of all the genes in the gbk file
     * @return list of all the found genes
     */
    private ArrayList fetchGene(final String input,
            final ArrayList<Gene> genes) {
        ArrayList<Gene> listOfGenes = new ArrayList<>();
        for (Gene gene : genes) {
            if (gene.getGeneName().contains(input.toUpperCase())) {
                listOfGenes.add(gene);
            }
        }
        System.out.println("There were " + listOfGenes.size() + " Genes found"
                + " with the given name pattern. A fasta with the information "
                + "is made at the place where program file is.");
        return listOfGenes;
    }
    /**
     * fetches the cds with a given product pattern.
     * @param input a product name
     * @param cdses all the cdses of the gbk file
     * @return a list of found cdses
     */
    private ArrayList fetchCds(final String input,
                                final ArrayList<Cds> cdses) {
        ArrayList<Cds> listOfCdses = new ArrayList<>();
        for (Cds cds : cdses) {
            if (cds.getProduct().contains(input)) {
                listOfCdses.add(cds);
            }
        }
        System.out.println("There were " + listOfCdses.size() + " Cdses found"
        + " with the given product pattern. A fasta with the information "
        + " is made at the place where program file is.");
        return listOfCdses;
    }
    /**
     * fetches all the features with given coordinates.
     * @param input coordinates
     * @param cdses a list of all the cdses in the gbk file
     * @param genes a list of all the genes in the gbk file
     * @return a list of all the found features
     */
    private ArrayList fetchFeatures(final String input,
                      final ArrayList<Cds> cdses, final ArrayList<Gene> genes) {
        ArrayList<Object> listOfFeatures = new ArrayList<>();
        try {
            String[] coords = input.split(",");
            for (Cds cds : cdses) {
                if (Integer.parseInt(cds.getCoordinate()[0])
                        > Integer.parseInt(coords[0])) {
                    if (Integer.parseInt(cds.getCoordinate()[1])
                            < Integer.parseInt(coords[1])) {
                        listOfFeatures.add(cds);
                    }
                }
            }
            for (Gene gene : genes) {
                if (Integer.parseInt(gene.getCoordinate()[0])
                        > Integer.parseInt(coords[0])) {
                    if (Integer.parseInt(gene.getCoordinate()[1])
                            < Integer.parseInt(coords[1])) {
                        listOfFeatures.add(gene);
                    }
                }
            }
        } catch (NumberFormatException ex) {
            System.out.println("You have enterd the coordinates wrong: " + input
                + "\nit must be seperated by a comma example: 20000,60000");
            System.exit(0);
        }
        return listOfFeatures;
    }
    /**
     * finds the sites in the organism with a given sequence.
     * @param input the input sequence
     * @param organism the organism of the gbk file
     * @param cdses the cdses found in the gbk file
     * @param genes the genes found in the gbk file
     * @return the sites that are found
     */
    private ArrayList findSites(final String input, final Organism organism,
            final ArrayList<Cds> cdses, final ArrayList<Gene> genes) {
        Map<String, String> dictionary = new HashMap<>();
        ArrayList<String> sites = new ArrayList<>();
        String newinput = input.toLowerCase();
        dictionary.put("r", "[ag]");
        dictionary.put("y", "[ct]");
        dictionary.put("m", "[ca]");
        dictionary.put("k", "[tg]");
        dictionary.put("w", "[ta]");
        dictionary.put("s", "[cg]");
        dictionary.put("b", "[ctg]");
        dictionary.put("d", "[atg]");
        dictionary.put("h", "[atc]");
        dictionary.put("v", "[acg]");
        dictionary.put("n", "[acgt]");
        for (String key : dictionary.keySet()) {
            newinput = newinput.replace(key, dictionary.get(key));
        }
        Pattern sitesRegex = Pattern.compile(newinput);
        Matcher sitesMatcher = sitesRegex.matcher(organism.getSequence());
        while (sitesMatcher.find()) {
            String sitesSeq = (organism.getSequence()
                    .substring(sitesMatcher.start(),
                    sitesMatcher.end()));
            String coordinate = sitesMatcher.start() + "," + sitesMatcher.end();
            String sitesInfo = "(" + coordinate + ");" + sitesSeq;
            String[] coords = coordinate.split(",");
            for (Cds cds : cdses) {
                if (Integer.parseInt(cds.getCoordinate()[0])
                        < Integer.parseInt(coords[0])) {
                    if (Integer.parseInt(cds.getCoordinate()[1])
                            > Integer.parseInt(coords[1])) {
                        sites.add(sitesInfo + ";Cds");
                    }
                }
            }
            for (Gene gene : genes) {
                if (Integer.parseInt(gene.getCoordinate()[0])
                        < Integer.parseInt(coords[0])
                        && Integer.parseInt(gene.getCoordinate()[1])
                        > Integer.parseInt(coords[1])) {
                        sites.add(sitesInfo + ";Gene");
                    } else {
                        if (!sites.contains(sitesInfo)) {
                            sites.add(sitesInfo);
                        }
                    }
                }
            }
        return sites;
    }
    /**
     * gets the list with all the information needed with the given option.
     * @return the list with the information needed with the given option
     */
    public final ArrayList<Object> getReturnList() {
        return returnList;
    }

}
