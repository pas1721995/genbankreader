package genbankreader;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Genes.
 *
 * @generated
 */
public class Gene {

    /**
     * The name of the gene.
     */
    private String geneName;
    /**
     * the coordinate of the gene.
     */
    private String[] coordinate;
    /**
     * is the sequence of the gene complementair.
     */
    private boolean complement;
    /**
     * the sequence of the gene.
     */
    private String sequence;
    /**
     * the constructor of the gene.
     * @param geneInfo the info of the gene
     * @param seq the sequence of the organism.
     */
    public Gene(final String geneInfo, final String seq) {
        setGeneName(geneInfo);
        setCoordinate(geneInfo);
        setSequence(coordinate, seq);
    }

    /**
     * sets the gene name.
     * @param info the info of the gene
     */
    private void setGeneName(final String info) {
        Pattern checkRegex = Pattern.compile("/locus_tag.*");
        Matcher regexMatcher = checkRegex.matcher(info);
        while (regexMatcher.find()) {
            String source = (info.substring(regexMatcher.start(),
                    regexMatcher.end()));
            geneName = (source.split("/locus_tag=")[1].trim());
            break;
        }
    }

    /**
     * sets the coordinate of the gene.
     * @param info the info of the gene
     */
    private void setCoordinate(final String info) {
        String coordinateInfo = info.split("/locus_tag")[0];
        coordinateInfo = coordinateInfo.split("gene")[1].trim();
        if (coordinateInfo.contains("complement")) {
            complement = true;
            coordinateInfo = coordinateInfo.split("complement\\(")[1];
            coordinateInfo = coordinateInfo.split("\\)")[0].replace(">", "");
            coordinate = coordinateInfo.split("\\.\\.");
        } else {
            complement = false;
            coordinate = coordinateInfo.split("\\.\\.");
        }
    }

    /**
     * sets the sequence of the gene.
     * @param coords the coordinate of the gene
     * @param seq the sequence of the organism
     */
    private void setSequence(final String[] coords, final String seq) {
        // waar je mee bezig was
        Map<String, String> dictionary = new HashMap<>();
        dictionary.put("a", "t");
        dictionary.put("t", "a");
        dictionary.put("g", "c");
        dictionary.put("c", "g");
        if (complement == true) {
            String geneseq = seq.substring(Integer.parseInt(coords[0]) - 1,
                    Integer.parseInt(coords[1]) - 1);
            String newstring = "";
            for (int i = 0; i < geneseq.length(); i++) {
                char cha = geneseq.charAt(i);
                newstring += dictionary.get(Character.toString(cha));
                sequence = new StringBuffer(newstring).reverse().toString();
            }
        } else {
            sequence = seq.substring(Integer.parseInt(coords[0]) - 1,
                    Integer.parseInt(coords[1]) - 1);
        }

    }

    @Override
    public final String toString() {
        return "Gene{" + "geneName=" + geneName + ", coordinate="
                + Arrays.toString(coordinate) + ", complement=" + complement
                + ", sequence=" + sequence + '}';
    }
    /**
     * gets the name of the gene.
     * @return the name of the gene
     */
    public String getGeneName() {
        return geneName;
    }
    /**
     * gets the coordinate of the gene.
     * @return the coordinate of the gene
     */
    public String[] getCoordinate() {
        return coordinate;
    }
    /**
     * gets the boolean if it is complement of the gene.
     * @return the boolean if it is complement of the gene
     */
    public boolean isComplement() {
        return complement;
    }
    /**
     * gets the sequence of the gene.
     * @return the sequence of the gene
     */
    public String getSequence() {
        return sequence;
    }

}
