package genbankreader;

import java.io.FileNotFoundException;
import java.util.Formatter;

/**
 * Class that creates a fasta and writes to it.
 */
public class FastaCreator {
    /**
     * The formatter that is used for writing.
     */
    private Formatter formatFile;
    /**
     * The name of the file where it needs to write to.
     */
    private final String name = "results";
    /**
     * Creates a fasta File.
     */
    public final void createFasta() {
        try {
            formatFile = new Formatter(name + ".fa");
        } catch (FileNotFoundException e) {
            System.out.println("cant create the file");
        }
    }
    /**
     * writes to the fasta file.
     * @param line The information that needs to be written to the fasta
     */
    public final void writeInTheFasta(final String line) {
        formatFile.format("%s" + "\n", line);
    }

    /**
     * closes the fasta where the results are in.
     */
    public final void closeFasta() {
        formatFile.close();
    }

}
