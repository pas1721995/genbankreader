package genbankreader;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * the organism object.
 * @author prfolkersma
 */
public class Organism {
    /**
     * The name of the organism.
     */
    private String organism;
    /**
     * the sequence of the organism.
     */
    private String sequence;
    /**
     * the constructor of the organism.
     * @param file the complete gbk file
     */
    public Organism(final String file) {
        setOrganism(file);
        setSequence(file);
    }
    /**
     * sets the organism name of the organism.
     * @param file the complete gbk file
     */
    private void setOrganism(final String file) {
        String source;
        Pattern checkRegex = Pattern.compile("SOURCE.*");
        Matcher regexMatcher = checkRegex.matcher(file);
        regexMatcher.find();
        source = (file.substring(regexMatcher.start(), regexMatcher.end()));
        organism = source.split("SOURCE")[1].trim();
    }
    /**
     * sets the sequence of the organism.
     * @param file the complete gbk file
     */
    private void setSequence(final String file) {
        String dna = file.split("ORIGIN")[1];
        String[] arr = dna.split("\\d");
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            result.append(arr[i]);
        }
        String seq = result.toString();
        seq = seq.replaceAll("\\s+", "").split("\\\\")[0];
        sequence = seq;
    }

    @Override
    public final String toString() {
        return "Organism{" + "organism=" + organism
                + ", sequence=" + sequence + '}';
    }
    /**
     * gets the organism name.
     * @return the organism name
     */
    public final String getOrganism() {
        return organism;
    }
    /**
     * gets the sequence of the organism.
     * @return the sequence of the organism
     */
    public final String getSequence() {
        return sequence;
    }

}

