package genbankreader;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * the main class of the program.
 * @author prfolkersma
 */
class GenBankReader {

    /**
     * the organism of the gbk file.
     */
    private Organism source;
    /**
     * the genes that the organism has.
     */
    private final ArrayList<Gene> genes = new ArrayList<>();
    /**
     * the cds that the organism has.
     */
    private final ArrayList<Cds> cdses = new ArrayList<>();
    /**
     * the main of genbankbrowser.
     *
     * @param args the arguments of the program
     */
    public static void main(final String[] args) {
        GenBankReader reader = new GenBankReader();
        reader.start(args);
    }
    /**
     * the start of the program.
     * @param args the arguments of the program
     */
    private void start(final String[] args) {
        CliParser cli = new CliParser(args);
        FileOpener opener = new FileOpener();
        String file = opener.openFile(cli.getFile());
        Organism organism = new Organism(file);
        source = organism;
        Pattern checkRegex = Pattern.compile("gene (.*?)/locus_tag(.*?)"
                                            + "locus_tag", Pattern.DOTALL);
        Matcher regexMatcher = checkRegex.matcher(file);
        while (regexMatcher.find()) {
            String geneInfo = (file.substring(regexMatcher.start(),
                    regexMatcher.end()));
            Gene gene = new Gene(geneInfo, organism.getSequence());
            genes.add(gene);
        }
        Pattern cdsRegex = Pattern.compile("CDS  (.*?)gene   |CDS  (.*?)ORIGIN",
                                            Pattern.DOTALL);
        Matcher cdsMatcher = cdsRegex.matcher(file);
        while (cdsMatcher.find()) {
            String cdsInfo = (file.substring(cdsMatcher.start(),
                                cdsMatcher.end()));
            Cds cds = new Cds(cdsInfo);
            cdses.add(cds);
        }
        Executer exe = new Executer(cli.getOption(),
                cli.getInput(),
                genes,
                organism,
                cdses);
        OutputGenerator writer = new OutputGenerator(exe.getReturnList(),
                cli.getOption(), source, cdses, genes);
    }

}
