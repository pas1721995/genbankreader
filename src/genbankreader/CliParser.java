package genbankreader;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * The CliParser class.
 * @author prfolkersma
 */
public class CliParser {
    /**
     * the file of the program.
     */
    private Path file;
    /**
     * the options of the program.
     */
    private String option;
    /**
     * the input of the program.
     */
    private String input;
    /**
     * the cliparser.
     * @param args the arguments of the program.
     */
    public CliParser(final String[] args) {
        HelpFormatter formatter = new HelpFormatter();
        Options options = new Options();
        options.addOption("h", "help", false, "displays help");
        options.addOption("i", "infile", true,
                "the gbk file you want to search in");
        options.addOption("c", "fetch_cds", true,
                "the option to fetch a cds given a name to search for");
        options.addOption("g", "fetch_gene", true,
                "the option to fetch a gene given a name to search for");
        options.addOption("f", "fetch_features", true,
                "the option to fetch the features givin coordinates example:"
                        + " 20000,60000");
        options.addOption("s", "find_sites", true,
                "the option to fetch all the sites giving a nucleotide sequence"
                        + " ambigu nucleotides are permitted");
        options.addOption("u", "summary", false,
                "the option to fetch the summary of the gbk file.");
        try {
            BasicParser bp = new BasicParser();
            CommandLine cl = bp.parse(options, args);
            if (cl.getOptions().length >= 3) {
                System.out.print("Your commandline givin is wrong: ");
                System.out.println(Arrays.toString(args));
                System.out.println("you can only do one option at a time");
                formatter.printHelp("MY TOOL", options);
                System.exit(0);
            } else if (cl.getOptions().length == 0) {
                formatter.printHelp("GenBankReader", options);
                System.exit(0);
            }
            if (cl.hasOption("h")) {
                formatter.printHelp("GenBankReader", options);
                System.exit(0);
            } else {
                String path = cl.getOptionValue("i");
                int index = path.lastIndexOf(("/"));
                if (path.substring(index + 1).endsWith(".gbk")) {
                    file = Paths.get(path.substring(0, index),
                        path.substring(index + 1));
                    if (cl.hasOption("c")) {
                        input = cl.getOptionValue("c");
                        option = "c";
                    } else if (cl.hasOption("g")) {
                        input = cl.getOptionValue("g");
                        option = "g";
                    } else if (cl.hasOption("f")) {
                        input = cl.getOptionValue("f");
                        option = "f";
                    } else if (cl.hasOption("s")) {
                        input = cl.getOptionValue("s");
                        option = "s";
                    } else if (cl.hasOption("u")) {
                        input = cl.getOptionValue("u");
                        option = "u";
                    } else {
                        System.out.print("You provided an ill-formatted "
                                + "Command Line: ");
                        System.out.println(Arrays.toString(args));
                        formatter.printHelp("MY TOOL", options);
                        System.exit(0);
                    }
                } else {
                    System.out.println("you have given a wrong file type it needs to be a gbk file");
                    System.exit(0);
                }
            }
        } catch (ParseException ex) {
            System.out.print("You provided an ill-formatted Command Line: ");
            System.out.println(Arrays.toString(args));
            formatter.printHelp("MY TOOL", options);
            System.exit(0);
        }
    }
    /**
     * gets the path of the file.
     * @return a path of the file
     */
    public final Path getFile() {
        return file;
    }
    /**
     * gets the option of the commandline.
     * @return the option of the commandline
     */
    public final String getOption() {
        return option;
    }
    /**
     * gets the input of the command line.
     * @return the input of the command line
     */
    public final String getInput() {
        return input;
    }

}
