/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genbankreader;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * the output generator class creates the output for the given options.
 * @author Pascal
 */
public class OutputGenerator {
    /**
     * writes to fasta.
     * @param resultsList the list that the executer returns.
     * @param option the option of the program
     * @param organism the organism of the gbk file
     * @param cdses the cdses found in the gbk file
     * @param genes the genes found in the gbk file
     */
    public OutputGenerator(final ArrayList resultsList,
            final String option,
            final Organism organism,
            final ArrayList cdses,
            final ArrayList genes) {
        if ("c".equals(option)) {
            cdses(resultsList);
        }
        if ("g".equals(option)) {
            genes(resultsList);
        }
        if ("f".equals(option)) {
            features(resultsList);
        }
        if ("s".equals(option)) {
            sites(resultsList);
        }
        if ("u".equals(option)) {
            summary(organism, cdses, genes);
        }
    }
    /**
     * creates the output for the cdses that are found.
     * @param resultsList the cdses that are found
     */
    private void cdses(final ArrayList<Cds> resultsList) {
        FastaCreator writer = new FastaCreator();
        writer.createFasta();
        for (Cds cds : resultsList) {
            writer.writeInTheFasta(">" + cds.getProduct() + "\n"
                    + cds.getTranslation());
        }
        writer.closeFasta();
    }
    /**
     * creates the output for the genes that are found.
     * @param resultsList the genes that are found
     */
    private void genes(final ArrayList<Gene> resultsList) {
        FastaCreator writer = new FastaCreator();
        writer.createFasta();
        for (Gene gene: resultsList) {
            writer.writeInTheFasta(">" + gene.getGeneName() + "\n"
                    + gene.getSequence());
        }
        writer.closeFasta();
    }
    /**
     * creates the output for the features that are found.
     * @param resultsList the features that are found
     */
    private void features(final ArrayList<Object> resultsList) {
        CommandLinePrinter clp = new CommandLinePrinter();
        for (Object genecds : resultsList) {
            if (genecds.toString().contains("Cds")) {
                Cds cds = (Cds) genecds;
                String cdsInfo = String.format("found a Cds with coordinate"
                        + " %s", Arrays.toString(cds.getCoordinate()));
                clp.printer(cdsInfo);

            } else {
                Gene gene = (Gene) genecds;
                String geneInfo = String.format("found a Gene with coordinate"
                        + " %s", Arrays.toString(gene.getCoordinate()));
                clp.printer(geneInfo);
            }
        }
    }
    /**
     * creates the output for the sites that are found.
     * @param resultsList the sites that are found
     */
    private void sites(final ArrayList<Object> resultsList) {
        CommandLinePrinter clp = new CommandLinePrinter();
        for (Object site : resultsList) {
            String sites = site.toString();
            String[] siteInfo = sites.split(";");
            if (siteInfo.length == 3) {
                String sites3 = String.format("Coordinate it is found = %s,"
                        + " actual dna sequence = %s,"
                        + " element it resides in = %s",
                        siteInfo[0], siteInfo[1], siteInfo[2]);
                clp.printer(sites3);
            } else {
                String sites2 = String.format("Coordinate it is found = %s,"
                        + " actual dna sequence = %s,"
                        + " element it resides in = None",
                        siteInfo[0], siteInfo[1]);
                clp.printer(sites2);
            }

        }
    }
    /**
     * Creates the summary.
     * @param organism the organism of the gbk file
     * @param cdses the cdses that are found in the gbk file
     * @param genes the genes taht are found int the gbk file
     */
    private void summary(final Organism organism,
            final ArrayList cdses, final ArrayList genes) {
        CommandLinePrinter clp = new CommandLinePrinter();
        String organismName = String.format("The name of the organism: %s",
                organism.getOrganism());
        clp.printer(organismName);
        String numberOfCdses = String.format("The number of cdses found in"
                + " the GBK file: %s", cdses.size());
        clp.printer(numberOfCdses);
        String numberOfGenes = String.format("The number of genes found in"
                + " the GBK file: %s", genes.size());
        clp.printer(numberOfGenes);
    }
}


