package genbankreader;

/**
 * The commandLinePrinter prints all the information to the terminal.
 * @author prfolkersma
 */
public class CommandLinePrinter {
    /**
     * prints all the features that are found.
     * @param line the line with the feature information
     */
    public final void printer(final String line) {
        System.out.println(line);
    }
}
